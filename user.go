package model

import "github.com/gin-gonic/gin"

type LikesModel struct {
	ActionType string `json:"action_type"` // 1-点赞，2-取消点赞
	Token      string `json:"token"`       // 用户鉴权token
	VideoID    string `json:"video_id"`    // 视频id
}
type VideoInfo struct {
	Id     int `json:"id"`
	Author struct {
		Id              int    `json:"id"`
		Name            string `json:"name"`
		FollowCount     int    `json:"follow_count"`
		FollowerCount   int    `json:"follower_count"`
		IsFollow        bool   `json:"is_follow"`
		Avatar          string `json:"avatar"`
		BackgroundImage string `json:"background_image"`
		Signature       string `json:"signature"`
		TotalFavorited  string `json:"total_favorited"`
		WorkCount       int    `json:"work_count"`
		FavoriteCount   int    `json:"favorite_count"`
	} `json:"author"`
	PlayUrl       string `json:"play_url"`
	CoverUrl      string `json:"cover_url"`
	FavoriteCount int    `json:"favorite_count"`
	CommentCount  int    `json:"comment_count"`
	IsFavorite    bool   `json:"is_favorite"`
	Title         string `json:"title"`
}

func LikeThisVideo(c *gin.Context) {
	//首先应该先获取服务器传来的LikesModel，解析这些参数
	//根据参数的定义，应该要在这一个函数里实现点赞和取消点赞这两种功能
}
func Push_Likelist(c *gin.Context) {

}
